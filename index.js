const http = require("http");
const url = require("url");

const PORT = process.env.PORT || 3000;
let fruits = [
  { name: "Apple", Qty: 28, color: "Red", isExpire: false },
  { name: "Mango", Qty: 30, color: "Yellow", isExpire: false },
  { name: "Grapes", Qty: 23, color: "Green", isExpire: false },
  { name: "Orange", Qty: 12, color: "Orange", isExpire: false },
];

const server = http.createServer((req, res) => {
  const query = url.parse(req.url, true).query.search?.toLocaleLowerCase();
  req.on("data", (chunk) => {
    bodyData = JSON.parse(chunk.toString());
    console.log(bodyData);
  });

  try {
    if (req.method === "GET") {
      if (req.url === "/") {
        res.end(`Server Running...`);
      } else if (req.url === "/all") {
        res.end(JSON.stringify(fruits));
      } else if (query) {
        const result = fruits.find(
          (fruit) => fruit.name.toLocaleLowerCase() === query
        );
        result ? res.end(JSON.stringify(result)) : res.end("Not Found");
      } else {
        res.end({ message: "endpoint not exists" });
      }
    }
  } catch (error) {
    console.log("error", JSON.stringify(error));
    res.end(JSON.stringify(error));
  }
});
server.listen(PORT, () => console.log("Server", PORT));
